﻿using aikidoka.avalonia.ViewModels;
using Avalonia;
using Avalonia.Markup.Xaml;

namespace aikidoka.avalonia.Views
{
    public class AttacksView : ReactiveUserControl<AttacksViewModel>
    {
        public AttacksView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
