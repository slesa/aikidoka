﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace aikidoka.avalonia.Views
{
    public class SettingsBarView : UserControl
    {
        public SettingsBarView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
