﻿using aikidoka.avalonia.ViewModels;
using Avalonia;
using Avalonia.Markup.Xaml;

namespace aikidoka.avalonia.Views
{
    public class TechnicsView : ReactiveUserControl<TechnicsViewModel>
    {
        public TechnicsView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
