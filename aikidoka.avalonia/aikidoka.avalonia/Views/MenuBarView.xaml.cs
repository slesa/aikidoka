﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace aikidoka.avalonia.Views
{
    public class MenuBarView : UserControl
    {
        public MenuBarView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
