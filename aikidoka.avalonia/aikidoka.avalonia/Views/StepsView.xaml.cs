﻿using aikidoka.avalonia.ViewModels;
using Avalonia;
using Avalonia.Markup.Xaml;

namespace aikidoka.avalonia.Views
{
    public class StepsView : ReactiveUserControl<StepsViewModel>
    {
        public StepsView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
