﻿using aikidoka.avalonia.ViewModels;
using Avalonia;
using Avalonia.Markup.Xaml;

namespace aikidoka.avalonia.Views
{
    public class PracticesView : ReactiveUserControl<PracticesViewModel>
    {
        public PracticesView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
