namespace aikidoka.avalonia.Models
{
    public class GradeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}