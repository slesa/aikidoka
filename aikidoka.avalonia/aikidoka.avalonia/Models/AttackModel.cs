﻿namespace aikidoka.avalonia.Models
{
    public class AttackModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }
}