﻿using System.Reactive;
using ReactiveUI;

namespace aikidoka.avalonia.Models
{
    public class MenuItem
    {
        public ReactiveCommand<Unit, Unit> Command { get; }
    }
}