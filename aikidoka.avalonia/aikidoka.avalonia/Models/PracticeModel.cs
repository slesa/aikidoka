﻿namespace aikidoka.avalonia.Models
{
    public class PracticeModel
    {
        public int Id { get; set; }
        public int Grade { get; set; }
        public string Attack { get; set; }
        public string Technic { get; set; }
        public string Form { get; set; }
        public string Video { get; set; }
    }
}