namespace aikidoka.avalonia.Models
{
    public interface ISettings
    {
        string Language { get; }
        string Grade { get; }
    }
}