﻿namespace aikidoka.avalonia.Models
{
    public class StepModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}