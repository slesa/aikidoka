﻿using Avalonia;
using Avalonia.Markup.Xaml;

namespace aikidoka.avalonia
{
    // https://github.com/reactiveui/ReactiveUI.Samples/blob/master/avalonia/ReactiveUI.Samples.Suspension/App.xaml.cs
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
