﻿using aikidoka.avalonia.Views;
using ReactiveUI;
using Splat;

namespace aikidoka.avalonia.ViewModels
{
    public class ContentViewModel : ReactiveObject, IScreen
    {
        public RoutingState Router { get; private set; }
        
        public ContentViewModel(IMutableDependencyResolver dependencyResolver = null, RoutingState testRouter = null)
        {
            Router = testRouter ?? new RoutingState();
            System.Diagnostics.Debug.WriteLine( $"ContentViewModel, DependencyResolver {dependencyResolver}" );
            System.Console.WriteLine( $"ContentViewModel, DependencyResolver {dependencyResolver}" );
            dependencyResolver = dependencyResolver ?? Locator.CurrentMutable;

            RegisterParts(dependencyResolver);
            Router.Navigate.Execute(new StepsViewModel(this));
        }

        void RegisterParts(IMutableDependencyResolver dependencyResolver)
        {
            dependencyResolver.RegisterConstant<IScreen>(this);

            dependencyResolver.Register<IViewFor<PracticesViewModel>>(() => new PracticesView());
            dependencyResolver.Register<IViewFor<AttacksViewModel>>(() => new AttacksView());
            dependencyResolver.Register<IViewFor<TechnicsViewModel>>(() => new TechnicsView());
            dependencyResolver.Register<IViewFor<StepsViewModel>>(() => new StepsView());
        }
        
    }
}