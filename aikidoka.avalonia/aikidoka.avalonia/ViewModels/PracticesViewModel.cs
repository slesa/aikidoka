using System.Collections.Generic;
using aikidoka.avalonia.Models;
using ReactiveUI;
using Splat;

namespace aikidoka.avalonia.ViewModels
{
    public class PracticesViewModel : ViewModelBase, IRoutableViewModel
    {
        public PracticesViewModel(ISettings settings=null, IScreen screen=null)
        {
            System.Console.WriteLine( $"PracticesViewModel, screen {screen}" );
            System.Console.WriteLine( $"PracticesViewModel, settings {settings}" );
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            var set = settings ?? Locator.Current.GetService<ISettings>();
            System.Console.WriteLine( $"PracticesViewModel, settings now {set}" );
            Practices = CreatePractices();
        }

        public string UrlPathSegment => "/practices";
        public IScreen HostScreen { get; }
        public IEnumerable<PracticeModel> Practices { get; }

        IEnumerable<PracticeModel> CreatePractices()
        {
            yield return new PracticeModel()
            {
                Id = 1,
                Grade = 1,
                Attack = "Ai hanmi katate dori",
                Technic = "Ikkyo",
                Form = "omote",
                // Image = "data/ai-hanmi-katatedori.png",
            };
            yield return new PracticeModel()
            {
                Id = 2,
                Grade = 1,
                Attack = "Ai hanmi katatedori",
                Technic = "Ikkyo",
                Form = "ura",
                // Image = "data/gyaku-hanmi-katatedori.png",
            };
            yield return new PracticeModel()
            {
                Id = 3,
                Grade = 2,
                Attack = "Ai hanmi katate dori",
                Technic = "Nikyo",
                Form = "omote",
                // Image = "data/gyaku-hanmi-katatedori.png",
            };
            yield return new PracticeModel()
            {
                Id = 4,
                Grade = 2,
                Attack = "Ai hanmi katate dori",
                Technic = "Nikyo",
                Form = "ura",
                // Image = "data/gyaku-hanmi-katatedori.png",
            };
        }
    }
}