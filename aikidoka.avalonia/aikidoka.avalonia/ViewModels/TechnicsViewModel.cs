using System.Collections.Generic;
using aikidoka.avalonia.Models;
using ReactiveUI;
using Splat;

namespace aikidoka.avalonia.ViewModels
{
    public class TechnicsViewModel : ViewModelBase, IRoutableViewModel
    {
        public TechnicsViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            Technics = CreateTechnics();
        }

        public string UrlPathSegment => "/technics";
        public IScreen HostScreen { get; }
        public IEnumerable<TechnicModel> Technics { get; }

        IEnumerable<TechnicModel> CreateTechnics()
        {
            yield return new TechnicModel()
            {
                Id = 1,
                Name = "Ikkyo",
                Description =
                    "Armstreckhebel, auch Ude-osae genannt. Die Technik wird als omote (vor dem Partner) und ura (hinter dem Partner) gelehrt.",
                // Image = "data/ai-hanmi-katatedori.png",
            };
            yield return new TechnicModel()
            {
                Id = 2,
                Name = "Nikyo",
                Description = "Auch Kote-mawashi genannt.",
                // Image = "data/gyaku-hanmi-katatedori.png",
            };
            yield return new TechnicModel()
            {
                Id = 3,
                Name = "Sankyo",
                Description = "Auch Kote-hineri genannt.",
                // Image = "data/gyaku-hanmi-katatedori.png",
            };
            yield return new TechnicModel()
            {
                Id = 4,
                Name = "Yonkyo",
                Description = "Auch Tebuki-osae genannt.",
                // Image = "data/gyaku-hanmi-katatedori.png",
            };
            yield return new TechnicModel()
            {
                Id = 5,
                Name = "Gokyo",
                Description = "Auch Ude-nobashi genannt.",
                // Image = "data/gyaku-hanmi-katatedori.png",
            };
        }
    }
}