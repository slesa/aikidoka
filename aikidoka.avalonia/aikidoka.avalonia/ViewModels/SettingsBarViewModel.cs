﻿using aikidoka.avalonia.Models;
using System.Collections.Generic;
using System.Linq;
using ReactiveUI;
using Splat;

namespace aikidoka.avalonia.ViewModels
{
    public class SettingsBarViewModel : ViewModelBase, ISettings
    {
        public SettingsBarViewModel(IMutableDependencyResolver dependencyResolver = null)
        {
            System.Diagnostics.Debug.WriteLine( $"SettingsBarViewModel, DependencyResolver {dependencyResolver}" );
            System.Console.WriteLine( $"SettingsBarViewModel, DependencyResolver {dependencyResolver}" );
            dependencyResolver = dependencyResolver ?? Locator.CurrentMutable;

            Languages = CreateLanguages();
            Language = Languages.First();
            Grades = CreateGrades();
            Grade = Grades.First();

            RegisterParts(dependencyResolver);
        }

        string _language;
        public string Language
        {
            get => _language;
            set
            {
                System.Diagnostics.Debug.WriteLine( $"SettingsBarViewModel, Language updated {value}" );
                System.Console.WriteLine( $"SettingsBarViewModel, Language updated {value}" );
                this.RaiseAndSetIfChanged(ref _language, value);
            }
        }

        public IEnumerable<string> Languages { get; }
        IEnumerable<string> CreateLanguages()
        {
            yield return "English";
            yield return "Deutsch";
            yield return "Français";
        }

        string _grade;
        public string Grade
        {
            get => _grade;
            set
            {
                System.Diagnostics.Debug.WriteLine( $"SettingsBarViewModel, Grade updated {value}" );
                System.Console.WriteLine( $"SettingsBarViewModel, Grade updated {value}" );
                this.RaiseAndSetIfChanged(ref _grade, value);
            }
        }

        public IEnumerable<string> Grades { get; }
        IEnumerable<string> CreateGrades()
        {
            yield return "All";
            yield return "5. Kyū";
            yield return "4. Kyū";
            yield return "3. Kyū";
            yield return "2. Kyū";
            yield return "1. Kyū";
            yield return "1. Dan";
        }

        void RegisterParts(IMutableDependencyResolver dependencyResolver)
        {
            dependencyResolver.Register<ISettings>(() => this);
        }
    }
}