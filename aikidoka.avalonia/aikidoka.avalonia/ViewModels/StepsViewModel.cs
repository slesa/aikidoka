﻿using System.Collections.Generic;
using aikidoka.avalonia.Models;
using ReactiveUI;
using Splat;

namespace aikidoka.avalonia.ViewModels
{
    public class StepsViewModel : ViewModelBase, IRoutableViewModel
    {
        public StepsViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            Steps = CreateSteps();
        }

        public string UrlPathSegment => "/steps";
        public IScreen HostScreen { get; }
        public IEnumerable<StepModel> Steps { get; }

        IEnumerable<StepModel> CreateSteps()
        {
            yield return new StepModel()
            {
                Id = 1,
                Name = "Irimi",
                Description =
                    "Irimi ist nur ein Schritt nach vorne. Allerdings wird damit auch ein Prinzip bezeichnet, auf das später eingegangen wird."
            };
            yield return new StepModel()
            {
                Id = 2,
                Name = "Tenkan",
                Description =
                    "Dabei wird ein Fuß zurückgenommen und man dreht sich um 180 Grad. Dies ist eine Ausweichbewegung, bei der die Kraft des Partners durchgelassen wird. Der vordere Fuß bleibt dabei möglichst unter Spannung, damit das Knie geschützt ist."
            };
            yield return new StepModel()
            {
                Id = 3,
                Name = "Kaiten",
                Description =
                    "Das Wort bedeutet Windmühle. Kaiten ist eine auf der Stelle ausgeführte Drehung um 180 Grad. Dabei ist zu beachten, dass die Kraft aus der Hüfte kommt, was ein scharfes und schnelles Drehen ermöglicht."
            };
        }
    }
}