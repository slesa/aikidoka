﻿using System.Reactive;
using System.Reactive.Linq;
using System.Windows.Input;
using ReactiveUI;

namespace aikidoka.avalonia.ViewModels
{
    public class MenuBarViewModel : ViewModelBase
    {
        public MenuBarViewModel(RoutingState router)
        {
            Router = router;

            var canPractices = this
                .WhenAnyObservable(x => x.Router.CurrentViewModel)
                .Select(current => !(current is PracticesViewModel));
            PracticesCommand = ReactiveCommand.Create(
                () => { return Router.Navigate.Execute(new PracticesViewModel()); },
                canPractices);            
            
            var canAttacks = this
                .WhenAnyObservable(x => x.Router.CurrentViewModel)
                .Select(current => !(current is AttacksViewModel));
            AttacksCommand = ReactiveCommand.Create(
                () => { return Router.Navigate.Execute(new AttacksViewModel()); },
                canAttacks);
            
            var canTechnics = this
                .WhenAnyObservable(x => x.Router.CurrentViewModel)
                .Select(current => !(current is TechnicsViewModel));
            TechnicsCommand = ReactiveCommand.Create(
                () => { return Router.Navigate.Execute(new TechnicsViewModel()); },
                canTechnics);

            var canSteps = this
                .WhenAnyObservable(x => x.Router.CurrentViewModel)
                .Select(current => !(current is StepsViewModel));
            StepsCommand = ReactiveCommand.Create(
                () => { return Router.Navigate.Execute(new StepsViewModel()); },
                canSteps);
            GlossarCommand = ReactiveCommand.Create(Glossar);
        }

        public RoutingState Router { get; }

        public ICommand PracticesCommand { get; }
        public ICommand AttacksCommand { get; }
        public ICommand TechnicsCommand { get; }
        public ICommand StepsCommand { get; }

        public ReactiveCommand<Unit, Unit> GlossarCommand { get; }
        void Glossar()
        {
            System.Diagnostics.Debug.WriteLine("Glossar");
        }
    }
}