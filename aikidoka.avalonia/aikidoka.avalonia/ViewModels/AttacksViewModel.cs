﻿using System.Collections.Generic;
using aikidoka.avalonia.Models;
using ReactiveUI;
using Splat;

namespace aikidoka.avalonia.ViewModels
{
    public class AttacksViewModel : ViewModelBase, IRoutableViewModel
    {
        public AttacksViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            Attacks = CreateAttacks();
        }

        public string UrlPathSegment => "/attacks";
        public IScreen HostScreen { get; }
        public IEnumerable<AttackModel> Attacks { get; }

        IEnumerable<AttackModel> CreateAttacks()
        {
            yield return new AttackModel()
            {
                Id = 1,
                Name = "Ai hanmi katate dori",
                Description =
                    "Die rechte Hand des einen umfasst das rechte Handgelenk des anderen bzw. die linke Hand das linke Handgelenk. Dies wird auch als harmonische Ausgangssituation bezeichnet, weil die Hand des einen die gleich starke Hand des anderen festhält.",
                Image = "data/ai-hanmi-katatedori.png",
            };
            yield return new AttackModel()
            {
                Id = 2,
                Name = "Gyaku hanmi katate dori",
                Description = "Die rechte Hand des einen umfasst das linke Handgelenk des anderen bzw. die linke Hand das rechte Handgelenk.",
                Image = "data/gyaku-hanmi-katatedori.png",
            };
        }
    }
}