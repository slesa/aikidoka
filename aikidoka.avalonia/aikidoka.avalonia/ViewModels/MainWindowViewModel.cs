﻿namespace aikidoka.avalonia.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            ContentViewModel = new ContentViewModel();

            MenuBarViewModel = new MenuBarViewModel(ContentViewModel.Router);
            SettingsBarViewModel = new SettingsBarViewModel();
        }

        public MenuBarViewModel MenuBarViewModel { get; }
        public SettingsBarViewModel SettingsBarViewModel { get; }

        public ContentViewModel ContentViewModel { get; }
    }
}
