using System;
using System.Globalization;
using Avalonia.Data.Converters;
using Avalonia.Media.Imaging;

namespace aikidoka.avalonia.Converters
{
	public class PathToBitmapConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object paramter, CultureInfo cultureInfo)
		{
			var path = (string) value;
			return new Bitmap(path);
		}

		public object ConvertBack(object value, Type targetType, object paramter, CultureInfo cultureInfo)
		{
			throw new NotImplementedException();
		}
	}
}