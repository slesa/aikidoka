module ServerTests.Tests

open Expecto
open ServerCode
open ServerCode.Storage
open ServerCode.Storage.Defaults


let aikidokaTests =
    testList "Aikidoka" [
        testCase "default forms contains suwari waza" (fun _ ->
            Expect.isNonEmpty defaultAikiForms "Default Forms list should have at least one item"
            Expect.isTrue
                (defaultAikiForms |> Seq.exists (fun b -> b.Name = "Suwari Waza")) 
                 "Suwari waza is part of the list"
        )
        testCase "default attacks contains ai hanmi" (fun _ ->
            Expect.isNonEmpty defaultAikiAttacks "Default Attacks list should have at least one item"
            Expect.isTrue
                (defaultAikiAttacks |> Seq.exists (fun b -> b.Name = "Ai Hanmi Katate Dori")) 
                 "Ai Hanmi is part of the list"
        )
        testCase "default moves contains ikkyo omote" (fun _ ->
            Expect.isNonEmpty defaultAikiMoves "Default Moves list should have at least one item"
            Expect.isTrue
                (defaultAikiMoves |> Seq.exists (fun b -> b.Name = "Ikkyo Omote")) 
                 "Ikkyo Omote is part of the list"
        )
    ]

(*
let defaults = ServerCode.Storage.Defaults.defaultWishList "test"

let wishListTests =
    testList "Wishlist" [
        testCase "default contains F# mastering book" (fun _ ->
            Expect.isNonEmpty defaults.Books "Default Books list should have at least one item"
            Expect.isTrue
                (defaults.Books |> Seq.exists (fun b -> b.Title = "Mastering F#")) 
                 "A good book should have been advertised"
        )

        testCase "adding a duplicate book is forbidden" (fun _ ->
            let duplicate = defaults.Books |> Seq.head
            let result = defaults.VerifyNewBookIsNotADuplicate duplicate
            Expect.isSome result "Can't add a duplicate"
        )
    ]
    *)