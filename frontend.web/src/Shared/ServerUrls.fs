/// API urls shared between client and server.
module ServerCode.ServerUrls

[<RequireQualifiedAccess>]
module PageUrls =
  [<Literal>]
  let Home = "/"
  [<Literal>]
  let Login = "/login"

[<RequireQualifiedAccess>]
module APIUrls =

  [<Literal>]
  let AikiForms = "/api/aikiforms/"
  [<Literal>]
  let AikiMoves = "/api/aikimoves/"
  [<Literal>]
  let AikiAttacks = "/api/aikiattacks/"
  [<Literal>]
  let AikiTechniques = "/api/aikitechs/"
  [<Literal>]
  let WishList = "/api/wishlist/"
  [<Literal>]
  let ResetTime = "/api/wishlist/resetTime/"
  [<Literal>]
  let Login = "/api/users/login/"
