/// Functions for managing the database.
module ServerCode.Database

open ServerCode.Storage.AzureTable
open ServerCode
open System.Threading.Tasks
open FSharp.Control.Tasks.ContextInsensitive

[<RequireQualifiedAccess>]
type DatabaseType =
    | FileSystem
    | AzureStorage of connectionString : AzureConnection

type IDatabaseFunctions =
    abstract member LoadAikiForms : string -> Task<Domain.AikiForms>
    abstract member LoadAikiAttacks : string -> Task<Domain.AikiAttacks>
    abstract member LoadAikiMoves : string -> Task<Domain.AikiMoves>
    abstract member LoadAikiTechniques : string -> Task<Domain.AikiTechniques>
    abstract member LoadAikiData : string -> Task<Domain.AikiData>
    abstract member LoadWishList : string -> Task<Domain.WishList>
    abstract member SaveWishList : Domain.WishList -> Task<unit>
    abstract member GetLastResetTime : unit -> Task<System.DateTime>

/// Start the web server and connect to database
let getDatabase databaseType startupTime =
    match databaseType with
    (*
    | DatabaseType.AzureStorage connection ->
        //Storage.WebJobs.startWebJobs connection
        { new IDatabaseFunctions with
            member __.LoadWishList key = Storage.AzureTable.getWishListFromDB connection key
            member __.SaveWishList wishList = Storage.AzureTable.saveWishListToDB connection wishList
            member __.GetLastResetTime () = task {
                let! resetTime = Storage.AzureTable.getLastResetTime connection
                return resetTime |> Option.defaultValue startupTime } }
    *)
    | DatabaseType.FileSystem ->
        { new IDatabaseFunctions with
            member __.LoadAikiForms key = task { 
                System.Console.WriteLine("Load aiki forms")
                let data = Storage.FileSystem.getAikiDataFromDB key
                return data.Forms }
            member __.LoadAikiAttacks key = task { 
                System.Console.WriteLine("Load aiki attacks")
                let data = Storage.FileSystem.getAikiDataFromDB key
                return data.Attacks }
            member __.LoadAikiMoves key = task { 
                System.Console.WriteLine("Load aiki moves")
                let data = Storage.FileSystem.getAikiDataFromDB key
                return data.Moves }
            member __.LoadAikiTechniques key = task { 
                System.Console.WriteLine("Load aiki techniques")
                let data = Storage.FileSystem.getAikiDataFromDB key
                return data.Techniques }
            member __.LoadAikiData key = task { return Storage.FileSystem.getAikiDataFromDB key }
            member __.LoadWishList key = task { return Storage.FileSystem.getWishListFromDB key }
            member __.SaveWishList wishList = task { return Storage.FileSystem.saveWishListToDB wishList }
            member __.GetLastResetTime () = task { return startupTime } }

