/// Wish list API web parts and data access functions.
module ServerCode.Aikidoka

open System.Threading.Tasks
open Microsoft.AspNetCore.Http
open Giraffe
open ServerCode.Domain
open ServerTypes
open FSharp.Control.Tasks.V2

/// Handle the GET on /api/aikiforms
let getAikiForms (getAikiFormsFromDB : string -> Task<AikiForms>) (token : UserRights) : HttpHandler =
     fun (next : HttpFunc) (ctx : HttpContext) ->
        task {
            let! aikiForms = getAikiFormsFromDB token.UserName
            return! ctx.WriteJsonAsync aikiForms
        }

/// Handle the GET on /api/aikimoves
let getAikiMoves (getAikiMovesFromDB : string -> Task<AikiMoves>) (token : UserRights) : HttpHandler =
     fun (next : HttpFunc) (ctx : HttpContext) ->
        task {
            let! aikiMoves = getAikiMovesFromDB token.UserName
            return! ctx.WriteJsonAsync aikiMoves
        }

/// Handle the GET on /api/aikiattacks
let getAikiAttacks (getAikiAttacksFromDB : string -> Task<AikiAttacks>) (token : UserRights) : HttpHandler =
     fun (next : HttpFunc) (ctx : HttpContext) ->
        task {
            let! aikiAttacks = getAikiAttacksFromDB token.UserName
            return! ctx.WriteJsonAsync aikiAttacks
        }

/// Handle the GET on /api/aikitechs
let getAikiTechniques (getAikiTechniquesFromDB : string -> Task<AikiTechniques>) (token : UserRights) : HttpHandler =
     fun (next : HttpFunc) (ctx : HttpContext) ->
        task {
            let! aikiTechs = getAikiTechniquesFromDB token.UserName
            return! ctx.WriteJsonAsync aikiTechs
        }
