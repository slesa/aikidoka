module ServerCode.Storage.Defaults

open ServerCode.Domain

/// The default initial data 
let defaultWishList userName =
    { UserName = userName
      Books = 
        [ { Title = "Mastering F#"
            Authors = "Alfonso Garcia-Caro Nunez"
            Link = "https://www.amazon.com/Mastering-F-Alfonso-Garcia-Caro-Nunez-ebook/dp/B01M112LR9" }
          { Title = "Get Programming with F#"
            Authors = "Isaac Abraham"
            Link = "https://www.manning.com/books/get-programming-with-f-sharp" } ] }

let defaultAikiGrades : AikiGrades =
  [ { Level = 6; Name = "6. Kyu" }
    { Level = 5; Name = "5. Kyu" }
    { Level = 4; Name = "4. Kyu" }
    { Level = 3; Name = "3. Kyu" }
    { Level = 2; Name = "2. Kyu" }
    { Level = 1; Name = "1. Kyu" }
  ]
let defaultAikiForms : AikiForms =
  [ { 
      Id = 1
      Name = "Suwari Waza"
      Origin = "座 り 技"
      Link = "http://a53.idata.over-blog.com/1/13/75/63/suwariwaza.gif"
      Description = "Le terme suwari waza est un terme japonais (座技) désignant les techniques martiales pratiquées à genoux. La notion est différente du ne waza qui sont également des techniques au sol, mais n'impliquant pas de déplacement (maîtrise de l'adversaire par immobilisation, strangulation ou luxation)." 
      Grade = 1
    }; { 
      Id = 2
      Name = "Tachi Waza"
      Origin = ""
      Link = "http://a54.idata.over-blog.com/1/13/75/63/tachiwaza.gif"
      Description = "" 
      Grade = 1
    }; { 
      Id = 3
      Name = "Ai Hanmi Handachi Waza"
      Origin = "半身半立技"
      Link = "http://a54.idata.over-blog.com/1/13/75/63/lex_hammihantachiwaza.gif"
      Description = "Ai hanmi Handachi Waza désigne la pratique de l'aïkido où Tori est à genoux et Uke est debout." 
      Grade = 1
    };
  ]
let defaultAikiAttacks : AikiAttacks =
    [ { 
        Id = 1
        Name = "Ai Hanmi Katate Dori"
        Origin = ""
        Link = "https://theaikidowarrior.files.wordpress.com/2016/06/kosadori-shihonage-omote-1.jpg"
        Description = "" 
        Grade = 1
      }; { 
        Id = 2
        Name = "Gyaku Hanmi Katate Dori"
        Origin = ""
        Link = "https://theaikidowarrior.files.wordpress.com/2016/06/kosadori-shihonage-omote-1.jpg"
        Description = "" 
        Grade = 1
      };
    ]
let defaultAikiMoves : AikiMoves =
    [ { 
        Id = 1
        Name = "Ikkyo Omote"
        Origin = ""
        Link = "https://theaikidowarrior.files.wordpress.com/2016/06/kosadori-shihonage-omote-1.jpg"
        Description = "" 
        Grade = 1
      }; { 
        Id = 2
        Name = "Ikkyo Ura"
        Origin = ""
        Link = "https://theaikidowarrior.files.wordpress.com/2016/06/kosadori-shihonage-omote-1.jpg"
        Description = "" 
        Grade = 1
      };
    ]
let defaultAikiTechniques : AikiTechniques =
  [ {
      Id = 1
      Form = 1
      Attack = 1
      Move = 1
      Link = ""
    }
  ]
let defaultAikiData : AikiData = {
  Forms = defaultAikiForms
  Attacks =  defaultAikiAttacks
  Moves = defaultAikiMoves
  Techniques = defaultAikiTechniques
  Grades = defaultAikiGrades
}
