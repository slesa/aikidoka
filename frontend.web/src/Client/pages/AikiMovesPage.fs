module Client.AikiMovesPage

open Fable.PowerPack
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Core.JsInterop

open Elmish
open Fetch.Fetch_types
open ServerCode
open ServerCode.Domain
open Client.Styles
open System
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

// DEMO02 - Models
type Model = { 
    // Domain data
    Moves : AikiMoves
    ErrorMsg : string option 
}

/// The different messages processed when interacting with the wish list
type Msg =
    | FetchedAikiMoves of AikiMoves
    | FetchError of exn

/// Get the wish list from the server, used to populate the model
let getAikiMoves token =
    promise {
        System.Console.WriteLine("Get aiki moves token")
        let url = ServerUrls.APIUrls.AikiMoves
        let props =
            [ Fetch.requestHeaders [
                HttpRequestHeaders.Authorization ("Bearer " + token) ]]

        let! res = Fetch.fetch url props
        let! txt = res.text()
        return Decode.Auto.unsafeFromString<AikiMoves> txt
    }


let init (user:UserData) =
    System.Console.WriteLine("init moves")
    { Moves = []
      ErrorMsg = None },
        Cmd.ofPromise getAikiMoves user.Token FetchedAikiMoves FetchError

let update (msg:Msg) model : Model * Cmd<Msg> =
    System.Console.WriteLine("updating moves")
    match msg with
    | FetchedAikiMoves moves ->
        let moves = moves |> List.sortBy (fun b -> b.Id)
        { model with Moves = moves }, Cmd.none
    | FetchError e ->
        { model with ErrorMsg = Some e.Message }, Cmd.none


let moveComponent (move:AikiMove) =
    tr [] [
        td [] [
            if String.IsNullOrWhiteSpace move.Link then
                yield str move.Name
            else
                yield a [ Href move.Link; Target "_blank" ] [str move.Name ] ]
        td [] [ str move.Origin ]
        td [] [ str move.Description ]
        td [] [ img [ Src move.Link ]]
    ]

let inline MoveComponent props = (ofFunction moveComponent) props []

// DEMO04 - React views - hot reloading
let view (model:Model) (dispatch: Msg -> unit) =
    div [ Key "WishList" ] [
        h4 [] [ str "Aikido Moves" ]
        table [ClassName "table table-striped table-hover"] [
            thead [] [
                tr [] [
                    th [] [str "Name"]
                    th [] [str "Origin"]
                    th [] [str "Description"]
                    th [] [str "Image"]
                ]
            ]
            tbody [] [
                model.Moves
                    |> List.map (fun move ->
                       MoveComponent move)
                    |> ofList
            ] 
        ]
        errorBox model.ErrorMsg
    ]
