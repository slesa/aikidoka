module Client.AikiAttacksPage

open Fable.PowerPack
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Core.JsInterop

open Elmish
open Fetch.Fetch_types
open ServerCode
open ServerCode.Domain
open Client.Styles
open System
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

// DEMO02 - Models
type Model = { 
    // Domain data
    Attacks : AikiAttacks
    ErrorMsg : string option 
}

/// The different messages processed when interacting with the wish list
type Msg =
    | FetchedAikiAttacks of AikiAttacks
    | FetchError of exn

/// Get the wish list from the server, used to populate the model
let getAikiAttacks token =
    promise {
        System.Console.WriteLine("Get aiki attacks token")
        let url = ServerUrls.APIUrls.AikiAttacks
        let props =
            [ Fetch.requestHeaders [
                HttpRequestHeaders.Authorization ("Bearer " + token) ]]

        let! res = Fetch.fetch url props
        let! txt = res.text()
        return Decode.Auto.unsafeFromString<AikiAttacks> txt
    }


let init (user:UserData) =
    System.Console.WriteLine("init attacks")
    { Attacks = []
      ErrorMsg = None },
        Cmd.ofPromise getAikiAttacks user.Token FetchedAikiAttacks FetchError

let update (msg:Msg) model : Model * Cmd<Msg> =
    System.Console.WriteLine("updating attacks")
    match msg with
    | FetchedAikiAttacks attacks ->
        let attacks = attacks |> List.sortBy (fun b -> b.Id)
        { model with Attacks = attacks }, Cmd.none
    | FetchError e ->
        { model with ErrorMsg = Some e.Message }, Cmd.none


let attackComponent (attack:AikiAttack) =
    tr [] [
        td [] [
            if String.IsNullOrWhiteSpace attack.Link then
                yield str attack.Name
            else
                yield a [ Href attack.Link; Target "_blank" ] [str attack.Name ] ]
        td [] [ str attack.Origin ]
        td [] [ str attack.Description ]
        td [] [ img [ Src attack.Link ]]
    ]

let inline AttackComponent props = (ofFunction attackComponent) props []

// DEMO04 - React views - hot reloading
let view (model:Model) (dispatch: Msg -> unit) =
    div [ Key "WishList" ] [
        h4 [] [ str "Aikido Attacks" ]
        table [ClassName "table table-striped table-hover"] [
            thead [] [
                tr [] [
                    th [] [str "Name"]
                    th [] [str "Origin"]
                    th [] [str "Description"]
                    th [] [str "Image"]
                ]
            ]
            tbody [] [
                model.Attacks
                    |> List.map (fun form ->
                       AttackComponent form)
                    |> ofList
            ] 
        ]
        errorBox model.ErrorMsg
    ]
