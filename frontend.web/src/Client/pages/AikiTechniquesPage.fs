module Client.AikiTechniquesPage

open Fable.PowerPack
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Core.JsInterop

open Elmish
open Fetch.Fetch_types
open ServerCode
open ServerCode.Domain
open Client.Styles
open System
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

// DEMO02 - Models
type Model = { 
    // Domain data
    Techniques : AikiTechniques
    ErrorMsg : string option 
}

/// The different messages processed when interacting with the wish list
type Msg =
    | FetchedAikiTechniques of AikiTechniques
    | FetchError of exn

/// Get the wish list from the server, used to populate the model
let getAikiTechniques token =
    promise {
        System.Console.WriteLine("Get aiki techniques token")
        let url = ServerUrls.APIUrls.AikiTechniques
        let props =
            [ Fetch.requestHeaders [
                HttpRequestHeaders.Authorization ("Bearer " + token) ]]

        let! res = Fetch.fetch url props
        let! txt = res.text()
        return Decode.Auto.unsafeFromString<AikiTechniques> txt
    }


let init (user:UserData) =
    System.Console.WriteLine("init techniques")
    { Techniques = []
      ErrorMsg = None },
        Cmd.ofPromise getAikiTechniques user.Token FetchedAikiTechniques FetchError

let update (msg:Msg) model : Model * Cmd<Msg> =
    System.Console.WriteLine("updating techniques")
    match msg with
    | FetchedAikiTechniques techs ->
        let techs = techs |> List.sortBy (fun b -> b.Id)
        { model with Techniques = techs }, Cmd.none
    | FetchError e ->
        { model with ErrorMsg = Some e.Message }, Cmd.none


let techComponent (tech:AikiTechnique) =
    tr [] [
        td [] [
            if String.IsNullOrWhiteSpace tech.Link then
                yield str ((tech.Id).ToString())
            else
                yield a [ Href tech.Link; Target "_blank" ] [str ((tech.Id).ToString()) ] ]
        td [] [ str ((tech.Form).ToString()) ]
        td [] [ str ((tech.Attack).ToString()) ]
        td [] [ str ((tech.Move).ToString()) ]
        //td [] [ str move.Description ]
        //td [] [ img [ Src move.Link ]]
    ]

let inline TechComponent props = (ofFunction techComponent) props []

// DEMO04 - React views - hot reloading
let view (model:Model) (dispatch: Msg -> unit) =
    div [ Key "WishList" ] [
        h4 [] [ str "Aikido Techniques" ]
        table [ClassName "table table-striped table-hover"] [
            thead [] [
                tr [] [
                    th [] [str "Link"]
                    th [] [str "Form"]
                    th [] [str "Attack"]
                    th [] [str "Move"]
                ]
            ]
            tbody [] [
                model.Techniques
                    |> List.map (fun tech ->
                       TechComponent tech)
                    |> ofList
            ] 
        ]
        errorBox model.ErrorMsg
    ]
