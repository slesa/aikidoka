module Client.AikiFormsPage

open Fable.PowerPack
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Core.JsInterop

open Elmish
open Fetch.Fetch_types
open ServerCode
open ServerCode.Domain
open Client.Styles
open System
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

// DEMO02 - Models
type Model = { 
    // Domain data
    Forms : AikiForms
    ErrorMsg : string option 
}

/// The different messages processed when interacting with the wish list
type Msg =
    | FetchedAikiForms of AikiForms
    | FetchError of exn

/// Get the wish list from the server, used to populate the model
let getAikiForms token =
    promise {
        System.Console.WriteLine("Get aiki forms token")
        let url = ServerUrls.APIUrls.AikiForms
        let props =
            [ Fetch.requestHeaders [
                HttpRequestHeaders.Authorization ("Bearer " + token) ]]

        let! res = Fetch.fetch url props
        let! txt = res.text()
        return Decode.Auto.unsafeFromString<AikiForms> txt
    }


let init (user:UserData) =
    System.Console.WriteLine("init forms")
    { Forms = []
      ErrorMsg = None },
        Cmd.ofPromise getAikiForms user.Token FetchedAikiForms FetchError

let update (msg:Msg) model : Model * Cmd<Msg> =
    System.Console.WriteLine("updating forms")
    match msg with
    | FetchedAikiForms forms ->
        let forms = forms |> List.sortBy (fun b -> b.Id)
        { model with Forms = forms }, Cmd.none
    | FetchError e ->
        { model with ErrorMsg = Some e.Message }, Cmd.none


let formComponent (form:AikiForm) =
    tr [] [
        td [] [
            if String.IsNullOrWhiteSpace form.Link then
                yield str form.Name
            else
                yield a [ Href form.Link; Target "_blank" ] [str form.Name ] ]
        td [] [ str form.Origin ]
        td [] [ str form.Description ]
        td [] [ img [ Src form.Link ]]
    ]

let inline FormComponent props = (ofFunction formComponent) props []

// DEMO04 - React views - hot reloading
let view (model:Model) (dispatch: Msg -> unit) =
    div [ Key "WishList" ] [
        h4 [] [ str "Aikido Forms" ]
        table [ClassName "table table-striped table-hover"] [
            thead [] [
                tr [] [
                    th [] [str "Name"]
                    th [] [str "Origin"]
                    th [] [str "Description"]
                    th [] [str "Image"]
                ]
            ]
            tbody [] [
                model.Forms
                    |> List.map (fun form ->
                       FormComponent form)
                    |> ofList
            ] 
        ]
        errorBox model.ErrorMsg
    ]
