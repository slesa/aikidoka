module Client.Pages

open Elmish.Browser.UrlParser

/// The different pages of the application. If you add a new page, then add an entry here.
[<RequireQualifiedAccess>]
type Page =
    | Home
    | Login
    | AikiForms
    | AikiMoves
    | AikiAttacks
    | AikiTechniques
    | WishList

let toPath =
    function
    | Page.Home -> "/"
    | Page.Login -> "/login"
    | Page.AikiForms -> "/forms"
    | Page.AikiMoves -> "/moves"
    | Page.AikiAttacks -> "/attacks"
    | Page.AikiTechniques -> "/techniques"
    | Page.WishList -> "/wishlist"


/// The URL is turned into a Result.
let pageParser : Parser<Page -> Page,_> =
    oneOf
        [ map Page.Home (s "")
          map Page.Login (s "login")
          map Page.AikiForms (s "forms")
          map Page.AikiMoves (s "moves")
          map Page.AikiAttacks (s "attacks")
          map Page.AikiTechniques (s "techniques")
          map Page.WishList (s "wishlist") ]

let urlParser location = parsePath pageParser location
