module Client.Shared

open ServerCode.Domain

type PageModel =
    | HomePageModel
    | LoginModel of Login.Model
    | WishListModel of WishList.Model
    | AikiFormsModel of Client.AikiFormsPage.Model
    | AikiAttacksModel of Client.AikiAttacksPage.Model
    | AikiMovesModel of Client.AikiMovesPage.Model
    | AikiTechniquesModel of Client.AikiTechniquesPage.Model

type Model = { 
    User : UserData option
    PageModel : PageModel 
}

/// The composed set of messages that update the state of the application
type Msg =
    | AikiFormsMsg of Client.AikiFormsPage.Msg
    | AikiAttacksMsg of Client.AikiAttacksPage.Msg
    | AikiMovesMsg of Client.AikiMovesPage.Msg
    | AikiTechniquesMsg of Client.AikiTechniquesPage.Msg
    | WishListMsg of WishList.Msg
    | LoginMsg of Login.Msg
    | LoggedIn of UserData
    | LoggedOut
    | StorageFailure of exn
    | Logout of unit


// VIEW

open Fable.Helpers.React
open Fable.Helpers.React.Props
open Client.Styles

// DEMO05 - the whole world put into a single view
let view model dispatch =
    div [ Key "Application" ] [
        Menu.view (Logout >> dispatch) model.User
        hr []

        div [ centerStyle "column" ] [
            match model.PageModel with
            | HomePageModel ->
                yield Home.view ()
            | LoginModel m ->
                yield Login.view m (LoginMsg >> dispatch)
            | WishListModel m ->
                yield WishList.view m (WishListMsg >> dispatch) 
            | AikiAttacksModel m ->
                yield Client.AikiAttacksPage.view m (AikiAttacksMsg >> dispatch) 
            | AikiFormsModel m ->
                yield Client.AikiFormsPage.view m (AikiFormsMsg >> dispatch) 
            | AikiMovesModel m ->
                yield Client.AikiMovesPage.view m (AikiMovesMsg >> dispatch) 
            | AikiTechniquesModel m ->
                yield Client.AikiTechniquesPage.view m (AikiTechniquesMsg >> dispatch) 
        ]
    ]
