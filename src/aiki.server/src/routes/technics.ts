import { NextFunction, Request, Response, Router } from "express";
import * as _ from 'lodash';

import { TechnicFactory } from '../model/technic-factory';
import { TechnicsStore } from '../technics-store';
import { HTTP } from './http';
import { NotificationService } from "../notification-service";

export class TechnicsRoute {

    public static create(router: Router, technicsStore: TechnicsStore, notificationService: NotificationService) {

        const technicsRoute = new TechnicsRoute(technicsStore, notificationService);
        const methodsToBind = [
            'getAll', 'getById', 'getAllBySearch']
            _.bindAll(technicsRoute, methodsToBind);

        router.get('/', technicsRoute.getAll);
        router.get('/:id', technicsRoute.getById);
        router.get('/search/:search', technicsStore.getAllBySearch);
    }

    constructor(
        private store: TechnicsStore,
        private notificationService: NotificationService
    ) {}

    getAll(req: Request, res: Response, next: NextFunction) {
        res.json(this.store.getAll());
        next();
    }

    getById(req: Request, res: Response, next: NextFunction) {
        const searchId = +req.params.id;
        res.json(this.store.getById(searchId));
        next();
    }

    getAllBySearch(req: Request, res: Response, next: NextFunction) {
        const searchTerm = req.params.search;
        res.json(this.store.getAllBySearch(searchTerm));
        next();
    }
};

