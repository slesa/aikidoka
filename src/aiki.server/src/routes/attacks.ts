import { NextFunction, Request, Response, Router } from "express";
import * as _ from 'lodash';

import { AttackFactory } from '../model/attack-factory';
import { AttacksStore } from '../attacks-store';
import { HTTP } from './http';
import { NotificationService } from "../notification-service";

export class AttacksRoute {

    public static create(router: Router, attacksStore: AttacksStore, notificationService: NotificationService) {

        const attacksRoute = new AttacksRoute(attacksStore, notificationService);
        const methodsToBind = [
            'getAll', 'getById', 'getAllBySearch']
            _.bindAll(attacksRoute, methodsToBind);

        router.get('/', attacksRoute.getAll);
        router.get('/:id', attacksRoute.getById);
        router.get('/search/:search', attacksStore.getAllBySearch);
    }

    constructor(
        private store: AttacksStore,
        private notificationService: NotificationService
    ) {}

    getAll(req: Request, res: Response, next: NextFunction) {
        res.json(this.store.getAll());
        next();
    }

    getById(req: Request, res: Response, next: NextFunction) {
        const searchId = +req.params.id;
        res.json(this.store.getById(searchId));
        next();
    }

    getAllBySearch(req: Request, res: Response, next: NextFunction) {
        const searchTerm = req.params.search;
        res.json(this.store.getAllBySearch(searchTerm));
        next();
    }
}