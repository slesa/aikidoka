import { NextFunction, Request, Response, Router } from "express";
import * as _ from 'lodash';

import { StepFactory } from '../model/step-factory';
import { StepsStore } from '../steps-store';
import { HTTP } from './http';
import { NotificationService } from "../notification-service";

export class StepsRoute {

    public static create(router: Router, stepsStore: StepsStore, notificationService: NotificationService) {
        
        const stepsRoute = new StepsRoute(stepsStore, notificationService);
        const methodsToBind = [
            'getAll', 'getById', 'getAllBySearch']
            _.bindAll(stepsRoute, methodsToBind);

            router.get('/', stepsRoute.getAll);
            router.get('/:id', stepsRoute.getById);
            router.get('/search/:search', stepsStore.getAllBySearch);
    }

    constructor(private store: StepsStore,
        private notificationService: NotificationService
        ) {}


    getAll(req: Request, res: Response, next: NextFunction) {
        res.json(this.store.getAll());
        next();
    }
            
                
    getById(req: Request, res: Response, next: NextFunction) {
        const searchId = +req.params.id;
        res.json(this.store.getById(searchId));
        next();
    }
            
    getAllBySearch(req: Request, res: Response, next: NextFunction) {
        const searchTerm = req.params.search;
        res.json(this.store.getAllBySearch(searchTerm));
        next();
    }
};