import { AttackFactory } from "../model/attack-factory";
import { TechnicFactory } from "../model/technic-factory";
import { BookFactory } from "../model/book-factory";

export const resolvers = {
  Query: {
    attack: (source, args, context) => context.store.getById(args.id),
    attacks: (source, args, context) => context.store.getAll(),
    attackSearch: (source, args, context) => context.store.getAllBySearch(args.searchTerm),
    technic: (source, args, context) => context.store.getById(args.id),
    technics: (source, args, context) => context.store.getAll(),
    technicSearch: (source, args, context) => context.store.getAllBySearch(args.searchTerm),
    step: (source, args, context) => context.store.getById(args.id),
    steps: (source, args, context) => context.store.getAll(),
    stepSearch: (source, args, context) => context.store.getAllBySearch(args.searchTerm),
    book: (source, args, context) => context.store.getByIsbn(args.isbn),
    books: (source, args, context) => context.store.getAll(),
    bookSearch: (source, args, context) => context.store.getAllBySearch(args.searchTerm),
    authors: (source, args, context) => context.store.getAllAuthors().map(name => ({ name })),
    isbnExists: (source, args, context) => context.store.isbnExists(args.isbn),
  },
  Book: {
    authors: (book) => book.authors.map(name => ({ name }))
  },
  Author: {
    books: (author, args, context) => context.store.findByAuthorName(author.name)
  },
  Mutation: {
    createBook: (source, args, context) => {
      const book = BookFactory.fromJson(args.book);
      context.store.create(book);
      return book;
    }
  }
}
