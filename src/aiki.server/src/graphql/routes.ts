import * as graphqlHTTP from 'express-graphql';
import { NextFunction, Request, Response, Router } from 'express';
import { BooksStore } from '../books-store';
import { AttacksStore } from '../attacks-store';
import { TechnicsStore } from '../technics-store';
import { StepsStore } from '../steps-store';
import { executableSchema } from './schema';

export class GraphQLRoute {

  public static createBooks(router: Router, bookStore: BooksStore) {
    router.use(graphqlHTTP({
      schema: executableSchema,
      graphiql: true,
      context: {
        store: bookStore
      }
    }));
  }

  public static createAttacks(router: Router, attacksStore: AttacksStore) {
    router.use(graphqlHTTP({
      schema: executableSchema,
      graphiql: true,
      context: {
        store: attacksStore
      }
    }));
  }

  public static createTechnics(router: Router, technicsStore: TechnicsStore) {
    router.use(graphqlHTTP({
      schema: executableSchema,
      graphiql: true,
      context: {
        store: technicsStore
      }
    }));
  }

  public static createSteps(router: Router, stepsStore: StepsStore) {
    router.use(graphqlHTTP({
      schema: executableSchema,
      graphiql: true,
      context: {
        store: stepsStore
      }
    }));
  }
}
