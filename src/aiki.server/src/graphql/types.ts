export const typeDefs = `
  type Attack {
    id: Int
    name: String
    description: String
    image: String
  }

  type Technic {
    id: Int
    name: String
    description: String
  }

  type Step {
    id: Int
    name: String
    description: String
  }

  type Query {
    attacks: [Attack]
    attack(id: Int): Attack
    attackSearch(searchTerm: String): [Attack]
    technics: [Technic]
    technic(id: Int): Technic
    technicSearch(searchTerm: String): [Technic]
    steps: [Step]
    step(id: Int): Step
    stepSearch(searchTerm: String): [Step]
    authors: [Author]
    books: [Book]
    book(isbn: ID!): Book
    isbnExists(isbn: ID!): Boolean
    bookSearch(searchTerm: String): [Book]
  }

  type Mutation {
    createBook(book: BookInput!): Book
  }

  type Book {
    isbn: ID!
    title: String
    subtitle: String
    rating: Int
    description: String
    thumbnails: [Thumbnail]
    authors: [Author]
  }

  type Thumbnail {
    url: String
    title: String
  }

  type Author {
    name: String,
    books: [Book]
  }

  input BookInput {
    isbn: String
    title: String
    subtitle: String
    rating: Int
    description: String
  }

  input ThumbnailInput {
    url: String
    title: String
  }
`
