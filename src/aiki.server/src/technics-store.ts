import * as _ from 'lodash';

import { Technic } from './model/technic';
import { TechnicFactory } from './model/technic-factory';
import { SomeTechnics } from './model/some-technics';
import { ENGINE_METHOD_DIGESTS } from 'constants';

export class TechnicsStore {

    private technicsCache = SomeTechnics.get();

    get technics(): Technic[] { 
        return this.technicsCache;
    }

    getAll(): Technic[] {
        return _(this.technics)
            .sortBy(t => t.id)
            .reverse()
            .value();
    }

    getById(id: number): Technic {
        return this.technics.find(t => t.id===id);
    }

    getAllBySearch(searchTerm: string): Technic[] {

        searchTerm = searchTerm.toLowerCase();
        const containsSearchTerm = (checked) => ~checked.toLowerCase().indexOf(searchTerm);
        return _(this.technics)
            .filter( a => 
                    !!(
                        containsSearchTerm(a.name) ||
                        containsSearchTerm(a.description)
                    )
                )
            .sortBy( a => a.id )
            .reverse()
            .value();
    }
}