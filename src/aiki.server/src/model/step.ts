import { AikiModel } from './aikimodel';

export class Step extends AikiModel {
    constructor(public id: number,
        public name: string,
        public description: string)
        { super(id, name, description); }
}