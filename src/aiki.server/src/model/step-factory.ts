import { Step } from './step';
import { BaseFactory } from './base-factory';

export class StepFactory {

    static empty(): Step {
        return new Step(0, '', '');
    }

    static fromJson(json: any): Step {

        let step = this.empty();

        if( BaseFactory.validNumber(json.id) )
            step.id = json.id;
        if( BaseFactory.validString(json.name) )
            step.name = name;
        if( BaseFactory.validString(json.description) )
            step.description = json.description;

        return step;
    }
}