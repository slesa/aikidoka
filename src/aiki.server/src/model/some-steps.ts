import { Step } from './step';

export class SomeSteps {

    public static get(): Step[] {
        return SomeSteps.steps;
    }

    public static steps: Step[] = [
        new Step(
            1,
            'Irimi',
            'Irimi ist nur ein Schritt nach vorne. Allerdings wird damit auch ein Prinzip bezeichnet, auf das später eingegangen wird.'
          ),
        new Step(
            2,
            'Tenkan',
            'Dabei wird ein Fuß zurückgenommen und man dreht sich um 180 Grad. Dies ist eine Ausweichbewegung, bei der die Kraft des Partners durchgelassen wird. Der vordere Fuß bleibt dabei möglichst unter Spannung, damit das Knie geschützt ist.'
          ),
        new Step(
            3,
            'Kaiten',
            'Das Wort bedeutet Windmühle. Kaiten ist eine auf der Stelle ausgeführte Drehung um 180 Grad. Dabei ist zu beachten, dass die Kraft aus der Hüfte kommt, was ein scharfes und schnelles Drehen ermöglicht.'
          )
        ]
};