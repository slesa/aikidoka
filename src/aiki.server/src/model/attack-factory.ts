import { Attack } from './attack';
import { BaseFactory } from './base-factory';

export class AttackFactory {

    static empty(): Attack {
        return new Attack(0, '', '', '');
    }

    static fromJson(json: any): Attack {

        let attack = AttackFactory.empty();

        if( BaseFactory.validNumber(json.id) )
            attack.id = json.id;
        if( BaseFactory.validString(json.name) )
            attack.name = json.name.trim();
        if( BaseFactory.validString(json.description) )
            attack.description = json.description.trim();
        if( BaseFactory.validString(json.image) )
            attack.image = json.image.trim();

        return attack;
    }
};