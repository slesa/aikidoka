import { Technic } from './technic';

export class SomeTechnics {

    public static get(): Technic[] {
        return SomeTechnics.technics;
    }

    public static technics: Technic[] = [
        new Technic(
            1,
            'Ikkyo',
            'Armstreckhebel, auch Ude-osae genannt. Die Technik wird als omote (vor dem Partner) und ura (hinter dem Partner) gelehrt.',
          ),
        new Technic(
            2,
            'Nikyo',
            'Auch Kote-mawashi genannt.',
          ),
        new Technic(
            3,
            'Sankyo',
            'Auch Kote-hineri genannt.',
          ),
        new Technic(
            4,
            'Yonkyo',
            'Auch Tebuki-osae genannt.',
          ),
        new Technic(
            5,
            'Gokyo',
            'Auch Ude-nobashi genannt.',
          )
    
    ]
};