import { Technic } from './technic';
import { BaseFactory } from './base-factory';

export class TechnicFactory {

    static empty(): Technic {
        return new Technic(0, '', '');
    }

    static fromJson(json: any): Technic {

        let technic = this.empty();

        if( BaseFactory.validNumber(json.id) )
            technic.id = json.id;
        if( BaseFactory.validString(json.name) )
            technic.name = json.name;
        if( BaseFactory.validString(json.description) )
            technic.description = json.description;
        return technic;
    }
}