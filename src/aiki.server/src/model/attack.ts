import { AikiModel } from './aikimodel';

export class Attack extends AikiModel {
    constructor(public id: number,
        public name: string,
        public description: string,
        public image: string) {
            super(id, name, description);
        }
}