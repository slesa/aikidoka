export class BaseFactory {
    public static normalizeRating(rating: number): number {
        let r = +rating;
        return (r < 0) ? 0 : (r > 5) ? 5 : r;
      }
    
    public static validString(str: string) {
        return str === '' || (str && typeof str == 'string');
      }
    
    public static validDate(date: string) {
        return (new Date(date)).toString() != 'Invalid Date';
      }
    
    public static validArray(arr: string) {
        return arr && Array.isArray(arr) && arr.length
      }
    
    public static validObject(obj) {
        return obj && typeof obj == 'object';
      }
    
    public static validNumber(no: string) {
        return no && typeof no == 'number';
      }
}