import { BaseModel } from './basemodel';

export class AikiModel extends BaseModel {
    constructor(public id: number,
        public name: string,
        public description: string) {
            super(id, name);
    }
}