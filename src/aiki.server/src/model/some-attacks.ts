import { Attack } from './attack';

export class SomeAttacks {
    public static get(): Attack[] {
        return SomeAttacks.attacks;
    }

    public static attacks: Attack[] = [
        new Attack(
            1,
            'Ai hanmi katate dori',
            'Die rechte Hand des einen umfasst das rechte Handgelenk des anderen bzw. die linke Hand das linke Handgelenk. Dies wird auch als harmonische Ausgangssituation bezeichnet, weil die Hand des einen die gleich starke Hand des anderen festhält.',
            '../../../aikidoka.avalonia/aikidoka.avalonia/data/ai-hanmi-katatedori.png'
          ),
        new Attack(
            2,
            'Gyaku hanmi katate dori',
            'Die rechte Hand des einen umfasst das linke Handgelenk des anderen bzw. die linke Hand das rechte Handgelenk.',
            '../../aikidoka.avalonia/aikidoka.avalonia/data/gyaku-hanmi-katatedori.png'
        )
    ]
};