import * as _ from 'lodash';

import { Step } from './model/step';
import { StepFactory } from './model/step-factory';
import { SomeSteps } from './model/some-steps';
import { ENGINE_METHOD_DIGESTS } from 'constants';

export class StepsStore {
    
    private stepsCache = SomeSteps.get();

    get steps(): Step[] {
        return this.stepsCache;
    }

    getAll(): Step[] {
        return _(this.steps)
            .sortBy(s => s.id)
            .reverse()
            .value();
    }

    getById(id: number): Step {
        return this.steps.find(s => s.id===id);
    }


    getAllBySearch(searchTerm: string): Step[] {

        searchTerm = searchTerm.toLowerCase();
        const containsSearchTerm = (checked) => ~checked.toLowerCase().indexOf(searchTerm);
        return _(this.steps)
            .filter( a => 
                    !!(
                        containsSearchTerm(a.name) ||
                        containsSearchTerm(a.description)
                    )
                )
            .sortBy( a => a.id )
            .reverse()
            .value();
    }
}