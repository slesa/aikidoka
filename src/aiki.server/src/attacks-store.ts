import * as _ from 'lodash';
import { Attack } from './model/attack';
import { AttackFactory } from './model/attack-factory';
import { SomeAttacks } from './model/some-attacks';

export class AttacksStore {

    private attacksCache = SomeAttacks.get();

    get attacks(): Attack[] {
        return this.attacksCache;
    }

    getAll(): Attack[] {
        return _(this.attacks)
            .sortBy(a => a.id)
            .reverse()
            .value();
    }

    getById(id: number): Attack {
        return this.attacks.find(attack => attack.id === id)
      };
    
    getAllBySearch(searchTerm: string): Attack[] {

        searchTerm = searchTerm.toLowerCase();
        const containsSearchTerm = (checked) => ~checked.toLowerCase().indexOf(searchTerm);
        return _(this.attacks)
            .filter( a => 
                    !!(
                        containsSearchTerm(a.name) ||
                        containsSearchTerm(a.description)
                    )
                )
            .sortBy( a => a.id )
            .reverse()
            .value();
    }
}
