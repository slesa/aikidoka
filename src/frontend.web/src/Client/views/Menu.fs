module Client.Menu

open Fable.Helpers.React
open Client.Styles
open Client.Pages
open ServerCode.Domain

//type Model = UserData option
type Model = AikiContext

let view onLogout (model:Model) =
    div [ centerStyle "row" ] [
        yield viewLink Page.Home "Home"
        yield viewLink Page.Settings "Settings"
        yield viewLink Page.AikiForms "Forms"
        yield viewLink Page.Aikidoka "Aikidoka"
        //if model <> None then
        yield viewLink Page.WishList "Wishlist"
        //if model = None then
        yield viewLink Page.Login "Login"
        //else
        //    yield buttonLink "logout" onLogout [ str "Logout" ]
    ]