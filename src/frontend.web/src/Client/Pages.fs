module Client.Pages

open Elmish.Browser.UrlParser

/// The different pages of the application. If you add a new page, then add an entry here.
[<RequireQualifiedAccess>]
type Page =
    | Home
    | Settings
    | AikiForms
    | Login
    | Aikidoka
    | WishList

let toPath =
    function
    | Page.Home -> "/"
    | Page.Settings -> "/settings"
    | Page.AikiForms -> "/forms"
    | Page.Login -> "/login"
    | Page.Aikidoka -> "/aikidoka"
    | Page.WishList -> "/wishlist"


/// The URL is turned into a Result.
let pageParser : Parser<Page -> Page,_> =
    oneOf
        [ map Page.Home (s "")
          map Page.Settings (s "settings") 
          map Page.AikiForms (s "forms") 
          map Page.Login (s "login")
          map Page.Aikidoka (s "aikidoka") 
          map Page.WishList (s "wishlist") ]

let urlParser location = parsePath pageParser location
