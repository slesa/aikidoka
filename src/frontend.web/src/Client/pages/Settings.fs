module Client.Settings

open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open ServerCode.Domain
open System
open Fable.Core.JsInterop
open Fable.PowerPack
open Fable.PowerPack.Fetch.Fetch_types
open ServerCode
open Client.Styles
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

type Model = {
    Settings : AikiSettings
    Running : bool
    ErrorMsg : string option }

// DEMO06 - messages everywhere - easy debugging
type Msg =
    | ConfirmSettings of AikiSettings
    | SetUserName of string
    | AuthError of exn
    | ConfirmClicked

let confirmUser (settings:AikiSettings) = promise {
    if String.IsNullOrEmpty settings.UserName then (*return!*) failwithf "You need to fill in a username."

    let body = Encode.Auto.toString(0, settings)

    let props = [ 
        RequestProperties.Method HttpMethod.POST
        Fetch.requestHeaders [ HttpRequestHeaders.ContentType "application/json" ]
        RequestProperties.Body !^body
    ]

    try
        // DEMO09 - using javascript APIs
        let! res = Fetch.fetch ServerUrls.APIUrls.AikiSettings props
        let! txt = res.text()
        return Decode.Auto.unsafeFromString<AikiSettings> txt
    with _ ->
        return! failwithf "Could not confirm settings."
}


let init (settings:AikiSettings) =
    //let userName = settings |> Option.map (fun u -> u.UserName) |> Option.defaultValue ""
    Console.WriteLine ("Username is " + settings.UserName) 
    { Settings = { UserName = settings.UserName; Grade = 0 }
      Running = false
      ErrorMsg = None }, Cmd.none
    
let update (msg:Msg) model : Model*Cmd<Msg> =
    match msg with
    | ConfirmSettings _ ->
        // DEMO07 - some messages are handled one level above
        model, Cmd.none 

    | SetUserName name ->
        { model with Settings = { UserName = name; Grade = model.Settings.Grade }}, Cmd.none

    | ConfirmClicked ->
        // DEMO08 - javascript promises
        { model with Running = true }, 
            Cmd.ofPromise confirmUser model.Settings ConfirmSettings AuthError

    | AuthError exn ->
        { model with Running = false; ErrorMsg = Some exn.Message }, Cmd.none

let view model (dispatch: Msg -> unit) =
    let buttonActive = 
        if String.IsNullOrEmpty model.Settings.UserName || model.Running
        then 
            "btn-disabled"
        else
            "btn-primary"
    
    div [ Key "SignIn"; ClassName "signInBox" ] [
        h3 [ ClassName "text-center" ] [ str "Aikidoka Personal Settings"]

        Styles.errorBox model.ErrorMsg

        div [ ClassName "input-group input-group-lg" ] [
            span [ClassName "input-group-addon" ] [
                span [ClassName "glyphicon glyphicon-user"] []
            ]
            input [
                Id "username"
                HTMLAttr.Type "text"
                ClassName "form-control input-lg"
                Placeholder "Username"
                DefaultValue model.Settings.UserName
                OnChange (fun ev -> dispatch (SetUserName ev.Value))
                AutoFocus true
            ]
        ]
(*
        div [ ClassName "input-group input-group-lg" ] [
            span [ClassName "input-group-addon" ] [
                span [ClassName "glyphicon glyphicon-asterisk"] []
            ]
            input [
                Id "password"
                Key ("password_" + model.Login.PasswordId.ToString())
                HTMLAttr.Type "password"
                ClassName "form-control input-lg"
                Placeholder "Password"
                DefaultValue model.Login.Password
                OnChange (fun ev -> dispatch (SetPassword ev.Value))
                onEnter LogInClicked dispatch
            ]
        ]
*)
        div [ ClassName "text-center" ] [
            button [ 
                ClassName ("btn " + buttonActive)
                OnClick (fun _ -> dispatch ConfirmClicked) ] [ 
                    str "Confirm" 
            ]
        ]
    ]
