module Client.AikiForms

open Fable.PowerPack
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Core.JsInterop

open Elmish
open Fetch.Fetch_types
open ServerCode
open ServerCode.Domain
open Client.Styles
open System
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

// DEMO02 - Models
type Model = { 
    // Domain data
    Forms : AikiForms
    ErrorMsg : string option 
}

/// The different messages processed when interacting with the wish list
type Msg =
    | FetchedAikiForms of AikiForms
    | FetchError of exn

/// Get the wish list from the server, used to populate the model
let getAikiForms token =
    promise {
        let url = ServerUrls.APIUrls.AikiForms
        let props =
            [ Fetch.requestHeaders [
                HttpRequestHeaders.Authorization ("Bearer " + token) ]]

        let! res = Fetch.fetch url props
        let! txt = res.text()
        return Decode.Auto.unsafeFromString<AikiForms> txt
    }


let init (user:UserData) =
    let submodel,cmd = NewBook.init()
    { WishList = WishList.New user.UserName
      Token = user.Token
      NewBookModel = submodel
      ResetTime = None
      ErrorMsg = None },
        Cmd.batch [
            Cmd.map NewBookMsg cmd
            Cmd.ofPromise getWishList user.Token FetchedWishList FetchError
            Cmd.ofPromise getResetTime user.Token FetchedResetTime FetchError ]

let update (msg:Msg) model : Model * Cmd<Msg> =
    match msg with
    | FetchedAikiForms aikiForms ->
        let aikiForms = { aikiForms with Forms = aikiForms.Forms |> List.sortBy (fun b -> b.Name) }
        { model with Forms = aikiForms }, Cmd.none

    | FetchError e ->
        { model with ErrorMsg = Some e.Message }, Cmd.none

type FormProps = { key: int; form: AikiForm }

let formComponent { form = form } =
    tr [] [
        td [] [ str form.Name ]
        td [] [ str form.Origin ]
        td [] [ str form.Description ]
        td [] [
            if String.IsNullOrWhiteSpace form.Link then
                yield str ""
            else
                yield a [ Href form.Link; Target "_blank" ] [str form.Name ] ]
    ]

let inline FormComponent props = (ofFunction formComponent) props []

// DEMO04 - React views - hot reloading
let view (model:Model) (dispatch: Msg -> unit) =
    div [ Key "WishList" ] [
        h4 [] [ str "Aikido Forms" ; str model.WishList.UserName; str time ]
        table [ClassName "table table-striped table-hover"] [
            thead [] [
                tr [] [
                    th [] [str "Name"]
                    th [] [str "Origin"]
                    th [] [str "Description"]
                    th [] [str "Link"]
                ]
            ]
            tbody [] [
                model.Forms
                    |> List.map (fun form ->
                       FormComponent {
                            key = form.Id
                            form = form
                       })
                    |> ofList
            ]
        ]
        //NewBook.view model.NewBookModel (dispatch << NewBookMsg)        
        errorBox model.ErrorMsg
    ]
