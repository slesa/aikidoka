module Client.Home

open Fable.Helpers.React
open Fable.Helpers.React.Props
open Client.Styles
//open Client.Pages

let view () =
    div [Key "Menu"; centerStyle "column"] [ 
        // viewLink Page.Login "Please login into the SAFE-Stack sample app"
        words 15 "Aikido ist eine betont defensive moderne japanische Kampfkunst, die Anfang des 20. Jahrhunderts von Morihei Ueshiba als Synthese verschiedener Aspekte unterschiedlicher Budo-Disziplinen, vor allem aber als Weiterentwicklung des Daito-Ryu Aiki-Jujutsu begründet wurde."
        br []
        words 15 "Aikidoka soll dem Lernenden bei der Prüfungsvorbereitung helfen, indem die Liste der Techniken angezeigt wird, die seinem derzeitigen Niveau entsprechen."
        br []
        a [ Href "https://www.aikido.com.fr/" ] [ img [ Src "/Images/ffaaa.png" ] ]
        br []
        br []
        br [] 
        br []
        br []
        words 20 ("version " + ReleaseNotes.Version) 
        br []
        br []
        words 20 "Made with"
        br []
        a [ Href "https://safe-stack.github.io/" ] [ img [ Src "/Images/safe_logo.png" ] ]
        //br []
        //br []
        //words 15 "An end-to-end, functional-first stack for cloud-ready web development that emphasises type-safe programming."
    ]
