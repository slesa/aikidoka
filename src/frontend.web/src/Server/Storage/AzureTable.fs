module ServerCode.Storage.AzureTable

open Microsoft.WindowsAzure.Storage
open Microsoft.WindowsAzure.Storage.Table
open ServerCode.Domain
open System
open System.Threading.Tasks
open FSharp.Control.Tasks.ContextInsensitive

type AzureConnection =
    | AzureConnection of string

(*
let getAikiTable (AzureConnection connectionString) tableName = task {
    let client = (CloudStorageAccount.Parse connectionString).CreateCloudTableClient()
    let table = client.GetTableReference tableName

    // Azure will temporarily lock table names after deleting and can take some time before the table name is made available again.
    let rec createTableSafe() = task {
        try
        let! _ = table.CreateIfNotExistsAsync()
        ()
        with _ ->
            do! Task.Delay 5000
            return! createTableSafe() }

    do! createTableSafe()
    return table }

let getAikiFormsTable connectionString = 
    getAikiTable connectionString "aikiforms"
let getAikiAttacksTable connectionString = 
    getAikiTable connectionString "aikiattacks"
let getAikiMovesTable connectionString =
    getAikiTable connectionString "aikimoves"
let getAikiTechniquesTable connectionString = 
    getAikiTable connectionString "aikitechs"
*)
let getBooksTable (AzureConnection connectionString) = task {
    let client = (CloudStorageAccount.Parse connectionString).CreateCloudTableClient()
    let table = client.GetTableReference "book"

    // Azure will temporarily lock table names after deleting and can take some time before the table name is made available again.
    let rec createTableSafe() = task {
        try
        let! _ = table.CreateIfNotExistsAsync()
        ()
        with _ ->
            do! Task.Delay 5000
            return! createTableSafe() }

    do! createTableSafe()
    return table }

(*
let getAikiEntryFromDB getAikiEntryFunc connectionString = task {
    let! table = getAikiEntryFunc connectionString
    let! results = table.ExecuteQuerySegmentedAsync(TableQuery(), null)
    return 
        [ for result in results -> {   
            Id = result.Properties.["Id"].IntValue
            Name = result.Properties.["Name"].StringValue
            Origin = result.Properties.["Origin"].StringValue
            Link = result.Properties.["Link"].StringValue
            Description = result.Properties.["Description"].StringValue
            Grade = result.Properties.["Grade"].IntValue
        } ]
}
let getAikiTechsFromDB connectionString = task {
    let! table = getAikiTechniquesTable connectionString
    let! results = table.ExecuteQuerySegmentedAsync(TableQuery(), null)
    return 
        [ for result in results -> {   
            Id = result.Properties.["Id"].IntValue
            Form = result.Properties.["Form"].IntValue
            Attack = result.Properties.["Attack"].IntValue
            Type = result.Properties.["Type"].StringValue
        } ]
}

/// Load from the database
let getAikiDataFromDB connectionString = task {
    let! forms = getAikiEntryFromDB getAikiFormsTable connectionString
    let! attacks = getAikiEntryFromDB getAikiAttacksTable connectionString
    let! moves = getAikiEntryFromDB getAikiMovesTable connectionString
    let! techniques = getAikiTechsFromDB connectionString
    return
        { Forms = forms
          Attacks =  attacks
          Moves = moves
          Techniques = techniques }
}
*)

let getWishListFromDB connectionString userName = task {
    let! results = task {
        let! table = getBooksTable connectionString
        let query = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, userName)
        return! table.ExecuteQuerySegmentedAsync(TableQuery(FilterString = query), null)  }
    return
        { UserName = userName
          Books =
            [ for result in results ->
                { Title = result.Properties.["Title"].StringValue
                  Authors = string result.Properties.["Authors"].StringValue
                  Link = string result.Properties.["Link"].StringValue } ] } }

/// Save to the database
let saveWishListToDB connectionString wishList = task {
    let buildEntity userName book =
        let isAllowed = string >> @"/\#?".Contains >> not
        let entity = DynamicTableEntity()
        entity.PartitionKey <- userName
        entity.RowKey <- book.Title.ToCharArray() |> Array.filter isAllowed |> String
        entity

    let! existingWishList = getWishListFromDB connectionString wishList.UserName
    let batch =
        let operation = TableBatchOperation()
        let existingBooks = existingWishList.Books |> Set
        let newBooks = wishList.Books |> Set

        // Delete obsolete books
        (existingBooks - newBooks)
        |> Set.iter(fun book ->
            let entity = buildEntity wishList.UserName book
            entity.ETag <- "*"
            entity |> TableOperation.Delete |> operation.Add)

        // Insert new / update existing books
        (newBooks - existingBooks)
        |> Set.iter(fun book ->
            let entity = buildEntity wishList.UserName book
            entity.Properties.["Title"] <- EntityProperty.GeneratePropertyForString book.Title
            entity.Properties.["Authors"] <- EntityProperty.GeneratePropertyForString book.Authors
            entity.Properties.["Link"] <- EntityProperty.GeneratePropertyForString book.Link
            entity |> TableOperation.InsertOrReplace |> operation.Add)

        operation

    let! booksTable = getBooksTable connectionString
    let! _ = booksTable.ExecuteBatchAsync batch
    () 
}

module StateManagement =
    let getStateBlob (AzureConnection connectionString) name = task {
        let client = (CloudStorageAccount.Parse connectionString).CreateCloudBlobClient()
        let state = client.GetContainerReference "state"
        let! _ = state.CreateIfNotExistsAsync()
        return state.GetBlockBlobReference name }

    let resetTimeBlob connectionString = getStateBlob connectionString "resetTime"

    let storeResetTime connectionString = task {
        let! blob = resetTimeBlob connectionString
        return! blob.UploadTextAsync "" }

let getLastResetTime connection = task {
    let! blob = StateManagement.resetTimeBlob connection
    do! blob.FetchAttributesAsync()
    return blob.Properties.LastModified |> Option.ofNullable |> Option.map (fun d -> d.UtcDateTime)
}
