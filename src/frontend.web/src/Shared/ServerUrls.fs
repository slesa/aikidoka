/// API urls shared between client and server.
module ServerCode.ServerUrls

[<RequireQualifiedAccess>]
module PageUrls =
  [<Literal>]
  let Home = "/"
  [<Literal>]
  let Settings = "/settings"
  [<Literal>]
  let Login = "/login"

[<RequireQualifiedAccess>]
module APIUrls =

  [<Literal>]
  let WishList = "/api/wishlist/"
  [<Literal>]
  let ResetTime = "/api/wishlist/resetTime/"
  [<Literal>]
  let Login = "/api/users/login/"

  [<Literal>]
  let AikiMenu = "/api/aikidoka/"
  [<Literal>]
  let AikiSettings = "/api/settings/"
  [<Literal>]
  let AikiForms = "/api/forms/"
  [<Literal>]
  let AikiAttacks = "/api/attacks/"
  [<Literal>]
  let AikiMoves = "/api/moves/"
  [<Literal>]
  let AikiTechniques = "/api/techniques/"
