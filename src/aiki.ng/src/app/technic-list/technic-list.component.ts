import { Component, OnInit } from '@angular/core';
import { Technic } from '../shared/technic';
import { TechnicsStoreService } from '../shared/technics-store.service';

@Component({
  selector: 'ad-technic-list',
  templateUrl: './technic-list.component.html',
  styleUrls: ['./technic-list.component.css']
})
export class TechnicListComponent implements OnInit {
  technics: Technic[];

  constructor(private technicsStore: TechnicsStoreService) { }

  ngOnInit() {
    this.technics = this.technicsStore.getAll();
  }

}
