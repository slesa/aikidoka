import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StepListComponent } from './step-list/step-list.component';
import { AttackListComponent } from './attack-list/attack-list.component';
import { TechnicListComponent } from './technic-list/technic-list.component';
import { AttacksStoreService } from './shared/attacks-store.service';
import { StepsStoreService } from './shared/steps-store.service';
import { TechnicsStoreService } from './shared/technics-store.service';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    StepListComponent,
    AttackListComponent,
    TechnicListComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    AttacksStoreService,
    StepsStoreService,
    TechnicsStoreService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
