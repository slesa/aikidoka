import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AttackListComponent } from './attack-list/attack-list.component';
import { StepListComponent } from './step-list/step-list.component';
import { TechnicListComponent } from './technic-list/technic-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'attacks',
    component: AttackListComponent
  },
  {
    path: 'steps',
    component: StepListComponent
  },
  {
    path: 'technics',
    component: TechnicListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
