import { Component, OnInit } from '@angular/core';
import { Attack } from '../shared/attack';
import { AttacksStoreService } from '../shared/attacks-store.service';

@Component({
  selector: 'ad-attack-list',
  templateUrl: './attack-list.component.html',
  styleUrls: ['./attack-list.component.css']
})
export class AttackListComponent implements OnInit {
  attacks: Attack[] = [];

  constructor(private attacksStore: AttacksStoreService) { }

  ngOnInit() {
    this.attacksStore.getAll().subscribe(res => this.attacks = res);
  }

}
