import { Injectable } from '@angular/core';
import { Technic } from './technic';

@Injectable({
  providedIn: 'root'
})
export class TechnicsStoreService {
  technics: Technic[];

  getAll(): Technic[] {
    return this.technics;
  }
  
  constructor() { 
    this.technics = [
      {
        id: 1,
        name: "Ikkyo",
        description: "Armstreckhebel, auch Ude-osae genannt. Die Technik wird als omote (vor dem Partner) und ura (hinter dem Partner) gelehrt.",
      },
      {
        id: 2,
        name: "Nikyo",
        description: "Auch Kote-mawashi genannt.",
      },
      { 
        id: 3,
        name: "Sankyo",
        description: "Auch Kote-hineri genannt.",
      },
      {
        id: 4,
        name: "Yonkyo",
        description: "Auch Tebuki-osae genannt.",
      },
      {
        id: 5,
        name: "Gokyo",
        description: "Auch Ude-nobashi genannt.",
      }
    ];
  }
}
