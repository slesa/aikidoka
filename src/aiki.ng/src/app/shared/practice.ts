import { Grade } from './grade';
import { Attack } from './attack';
import { Technic } from './technic';

export interface Practice {
    id: number;
    grade: Grade;
    attack: Attack;
    technic: Technic;
    form: string;
    video: string;
}
