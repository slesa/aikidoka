import { TestBed } from '@angular/core/testing';

import { TechnicsStoreService } from './technics-store.service';

describe('TechnicsStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TechnicsStoreService = TestBed.get(TechnicsStoreService);
    expect(service).toBeTruthy();
  });
});
