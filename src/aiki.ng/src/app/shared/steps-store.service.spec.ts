import { TestBed } from '@angular/core/testing';

import { StepsStoreService } from './steps-store.service';

describe('StepsStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepsStoreService = TestBed.get(StepsStoreService);
    expect(service).toBeTruthy();
  });
});
