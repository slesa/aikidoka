import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Attack } from './attack'
import { ServiceBase } from './service-base'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AttacksStoreService {
  // attacks: Attack[];

  getAll(): Observable<Attack[]> {
    return this.http.get<any[]>(`${ServiceBase.api}/attacks`);
  }
  
  constructor(private http: HttpClient) { 
    /* this.attacks = [
      {
        id: 1,
        name: "Ai hanmi katate dori",
        description: "Die rechte Hand des einen umfasst das rechte Handgelenk des anderen bzw. die linke Hand das linke Handgelenk. Dies wird auch als harmonische Ausgangssituation bezeichnet, weil die Hand des einen die gleich starke Hand des anderen festhält.",
        image: "../../../aikidoka.avalonia/aikidoka.avalonia/data/ai-hanmi-katatedori.png"
      },
      {
        id: 2,
        name: "Gyaku hanmi katate dori",
        description: "Die rechte Hand des einen umfasst das linke Handgelenk des anderen bzw. die linke Hand das rechte Handgelenk.",
        image: "../../aikidoka.avalonia/aikidoka.avalonia/data/gyaku-hanmi-katatedori.png"
      }
    ]; */
  }
}
