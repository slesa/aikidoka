import { Aikimodel } from './aikimodel';

export interface Attack extends Aikimodel {
    image: string;
}
