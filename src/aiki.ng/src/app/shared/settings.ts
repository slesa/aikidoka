import { Grade } from './grade';

export interface Settings {
    language: string;
    grade: Grade;
}
