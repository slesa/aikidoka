import { Basemodel} from './basemodel';

export interface Aikimodel extends Basemodel {
    description: string;
}
