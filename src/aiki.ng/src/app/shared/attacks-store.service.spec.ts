import { TestBed } from '@angular/core/testing';

import { AttacksStoreService } from './attacks-store.service';

describe('AttacksService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AttacksStoreService = TestBed.get(AttacksStoreService);
    expect(service).toBeTruthy();
  });
});
