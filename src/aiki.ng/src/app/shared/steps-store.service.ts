import { Injectable } from '@angular/core';
import { Step } from './step';

@Injectable({
  providedIn: 'root'
})
export class StepsStoreService {
  steps: Step[];

  getAll(): Step[] {
    return this.steps;
  }
  
  constructor() { 
    this.steps = [
      {
        id: 1,
        name: "Irimi",
        description: "Irimi ist nur ein Schritt nach vorne. Allerdings wird damit auch ein Prinzip bezeichnet, auf das später eingegangen wird."
      },
      {
        id: 2,
        name: "Tenkan",
        description: "Dabei wird ein Fuß zurückgenommen und man dreht sich um 180 Grad. Dies ist eine Ausweichbewegung, bei der die Kraft des Partners durchgelassen wird. Der vordere Fuß bleibt dabei möglichst unter Spannung, damit das Knie geschützt ist."
      },
      {
        id: 3,
        name: "Kaiten",
        description: "Das Wort bedeutet Windmühle. Kaiten ist eine auf der Stelle ausgeführte Drehung um 180 Grad. Dabei ist zu beachten, dass die Kraft aus der Hüfte kommt, was ein scharfes und schnelles Drehen ermöglicht."
      }
    ];
  }
}
