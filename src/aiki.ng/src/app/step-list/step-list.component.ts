import { Component, OnInit } from '@angular/core';
import { Step } from '../shared/step';
import { StepsStoreService } from '../shared/steps-store.service';

@Component({
  selector: 'ad-step-list',
  templateUrl: './step-list.component.html',
  styleUrls: ['./step-list.component.css']
})
export class StepListComponent implements OnInit {
  steps: Step[];

  constructor(private stepsStore: StepsStoreService) { }

  ngOnInit() {
    this.steps = this.stepsStore.getAll();
  }

}
