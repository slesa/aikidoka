# Aikidoka

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## History

### Create app

```
ng new Aikidoka -p ad (CSS)
```

### Semantic CSS

```
npm install semantic-ui-css
```

angular.json, architect, build, options, styles:
```
"styles": [
    "node_modules/semantic-ui-css/semantic.css"
]

### Loading sequence

index.html:
```
<ad-root>
    <div class="ui active inverted dimmer">
        <div class="ui text loaded large">Lade Aikidoka...</div>
    </div>
</ad-root>
```

### Creating models

```
ng g interface shared/settings
ng g interface shared/basemodel
ng g interface shared/aikimodel
ng g interface shared/grade
ng g interface shared/attack
ng g interface shared/step
ng g interface shared/technic
ng g interface shared/practice
```

### Creating component list

```
ng g component step-list
ng g component attack-list
```
