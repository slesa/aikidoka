--
-- File generated with SQLiteStudio v3.2.1 on Di Aug 20 17:25:50 2019
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: attacks
DROP TABLE IF EXISTS attacks;
CREATE TABLE attacks (id INT PRIMARY KEY UNIQUE NOT NULL, name TEXT (32) UNIQUE NOT NULL, descr VARCHAR (32));
INSERT INTO attacks (id, name, descr) VALUES (1, 'Ai hanmi katatedori', 'Die rechte Hand des einen umfasst das rechte Handgelenk des anderen bzw. die linke Hand das linke Handgelenk. Dies wird auch als harmonische Ausgangssituation bezeichnet, weil die Hand des einen die gleich starke Hand des anderen festhält.');
INSERT INTO attacks (id, name, descr) VALUES (2, 'Gyaku hanmi katatedori', 'Die rechte Hand des einen umfasst das linke Handgelenk des anderen bzw. die linke Hand das rechte Handgelenk.');
INSERT INTO attacks (id, name, descr) VALUES (7, 'Chudan-zuki', 'Stoß des einen mit der rechten bzw. linken Faust gegen den Magen des anderen. Meistens wird die Seite angegriffen, bei der das Bein vorne ist, wie bei der 2. Form.');
INSERT INTO attacks (id, name, descr) VALUES (6, 'Yokomen-uchi', 'Schlag von einem der Partner, der mit der rechten Hand von oben gegen linke Kopfseite des anderen bzw. mit der linken Hand gegen die rechte Kopfseite geführt wird. Dabei wird – wie bei der 2. Form – die Seite angegriffen, bei der das Bein vorne ist.');
INSERT INTO attacks (id, name, descr) VALUES (5, 'Shomen-uchi', 'Frontaler Schlag des einen von oben gegen den Kopf des anderen. Dabei ist zu beachten, dass die hintere Seite des Partners, also bei der das Bein hinten ist, angegriffen wird. So wie bei der 1. Form ist es wechselseitig.');
INSERT INTO attacks (id, name, descr) VALUES (4, 'Munedori', 'Die rechte bzw. die linke Hand des einen fasst die Kleidung des anderen über der Brust.');
INSERT INTO attacks (id, name, descr) VALUES (3, 'Katadori (-men-uchi)', 'Die rechte Hand des einen fasst die linke Schulter des anderen bzw. die linke Hand die rechte Schulter. Mit der freien Hand führt Ersterer einen frontalen Schlag von oben gegen den Kopf des Partners aus. Das men uchi steht in Klammern, da es nicht bei allen Techniken zu diesem Schlag kommt: Bei den Hebeln bewegt man sich, bevor der Schlag ausgeführt werden kann.');
INSERT INTO attacks (id, name, descr) VALUES (16, 'Ushiro-katate-kubi-shime-katate-(tekubi-)tori', 'Die rechte Hand des Angreifers würgt von hinten den Hals des Verteidigers, während seine linke Hand das linke Handgelenk des Partners umfasst, bzw. die linke Hand des einen würgt von hinten den Hals des anderen, während seine rechte Hand das rechte Handgelenk umfasst.');
INSERT INTO attacks (id, name, descr) VALUES (15, 'Ushiro-ryokatadori', 'Beide Hände des einen fassen von hinten die Schultern des anderen, was wiederum der 13. Form ähnelt.');
INSERT INTO attacks (id, name, descr) VALUES (14, 'Ushiro-ryohijidori', 'Beide Hände des Angreifers umfassen von hinten die Ellenbogengelenke des Verteidigers. Diese Form ist wieder ähnlich wie 13.');
INSERT INTO attacks (id, name, descr) VALUES (13, 'Ushiro-ryotedor', 'Beide Hände des einen umfassen von hinten die Handgelenke des Partners. Dies wird manchmal so geübt, dass der Partner wirklich von hinten kommt, doch meistens kommt er von vorne. Dazu bietet der Verteidiger – wie in der 9. Form – wieder eine Hand etwas höher an. Der Angreifer schneidet diese nach unten und fasst. Dann läuft er weiter um den Partner herum und versucht, die zweite Hand zu greifen.');
INSERT INTO attacks (id, name, descr) VALUES (12, 'Ryo-katadori', 'Beide Hände des einen fassen von vorne die Schultern des anderen.');
INSERT INTO attacks (id, name, descr) VALUES (11, 'Ryo-hijidori', 'Beide Hände des Angreifers umfassen von vorne die Ellenbogengelenke des Verteidigers.');
INSERT INTO attacks (id, name, descr) VALUES (10, 'Ryotedori', 'Jede Hand des einen umfasst von vorne eines der Handgelenke des anderen.');
INSERT INTO attacks (id, name, descr) VALUES (9, 'Katate-ryotedori', 'Beide Hände des einen umfassen rechten bzw. linken Unterarm des anderen. Dabei wird die Hand etwas höher angeboten. Der Angreifer schneidet diese mit einer Hand nach unten, greift zu und fasst darauf mit der anderen nach. Dies geschieht fast zeitgleich. Bei diesem Angriff kann der Angreifer die gefasste Hand leicht nach außen drehen. Hier sieht man noch deutlich den Bezug zum Schwert: Damit soll nämlich bewirkt werden, dass der Verteidiger sein Schwert mit der Hand nicht mehr ziehen kann.');
INSERT INTO attacks (id, name, descr) VALUES (8, 'Ushiro-katate-eridori', 'Die rechte bzw. linke Hand des einen fasst von hinten den Kragen des anderen. Dabei läuft er von vorne um das vordere Bein des Partners und greift in den Kragen.');

-- Table: grades
DROP TABLE IF EXISTS grades;
CREATE TABLE grades (id INT PRIMARY KEY UNIQUE NOT NULL, name TEXT (16) NOT NULL UNIQUE);
INSERT INTO grades (id, name) VALUES (6, '1. Dan');
INSERT INTO grades (id, name) VALUES (5, '1. Kyū');
INSERT INTO grades (id, name) VALUES (4, '2. Kyū');
INSERT INTO grades (id, name) VALUES (3, '3. Kyū');
INSERT INTO grades (id, name) VALUES (2, '4. Kyū');
INSERT INTO grades (id, name) VALUES (1, '5. Kyū');

-- Table: steps
DROP TABLE IF EXISTS steps;
CREATE TABLE steps (id INTEGER PRIMARY KEY UNIQUE NOT NULL, name TEXT (16) UNIQUE NOT NULL, descr VARCHAR (32));
INSERT INTO steps (id, name, descr) VALUES (1, 'Irimi', 'Irimi ist nur ein Schritt nach vorne. Allerdings wird damit auch ein Prinzip bezeichnet, auf das später eingegangen wird.');
INSERT INTO steps (id, name, descr) VALUES (2, 'Tenkan', 'Dabei wird ein Fuß zurückgenommen und man dreht sich um 180 Grad. Dies ist eine Ausweichbewegung, bei der die Kraft des Partners durchgelassen wird. Der vordere Fuß bleibt dabei möglichst unter Spannung, damit das Knie geschützt ist.');
INSERT INTO steps (id, name, descr) VALUES (3, 'Kaiten', 'Das Wort bedeutet Windmühle. Kaiten ist eine auf der Stelle ausgeführte Drehung um 180 Grad. Dabei ist zu beachten, dass die Kraft aus der Hüfte kommt, was ein scharfes und schnelles Drehen ermöglicht.');
INSERT INTO steps (id, name, descr) VALUES (4, 'Suri Ashi', 'Suri Ashi bezeichnet einen Schiebeschritt, bei dem der vordere Fuß vorne bleibt bzw. nach vorne geschoben und der hintere hinterhergezogen wird. Dieser Schiebeschritt ist der einzige, den man im Aikido benötigt. Es gibt noch eine zweite Schiebeschritt-Art, bei der der hintere Fuß unter den Schwerpunkt gezogen und dann der vordere nach vorne gesetzt wird. Diese ist z.B. beim Karate gebräuchlicher. Sie kann verwendet werden, um mit dem vorderen Bein zu treten.');
INSERT INTO steps (id, name, descr) VALUES (5, 'Ayumi-ashi', 'Ayumi-ashi ist im Aikido eher ungebräuchlich. Es bezeichnet einen Übersetzschritt. Der Schwerpunkt kann tief gehalten werden, der hintere Fuß kreuzt den vorderen und dann wird der zuerst vordere wieder nach vorne gesetzt.');
INSERT INTO steps (id, name, descr) VALUES (6, 'Tai Sabaki', 'Tai Sabaki heißt soviel wie „Körper bewegen“ und stellt die wichtigste Technik im Aikido dar, weil alle „Anfänger-Techniken“ diese Schrittkombination aus Irimi und Tenkan enthalten. Sie dient sowohl zum Ausweichen als auch zum Positionieren.');

-- Table: technics
DROP TABLE IF EXISTS technics;
CREATE TABLE technics (id INT PRIMARY KEY UNIQUE NOT NULL, name TEXT (32) NOT NULL UNIQUE, descr VARCHAR (32));
INSERT INTO technics (id, name, descr) VALUES (22, 'Kubi-nage', NULL);
INSERT INTO technics (id, name, descr) VALUES (21, 'Tai-otoshi', NULL);
INSERT INTO technics (id, name, descr) VALUES (20, 'Hijikime osae', NULL);
INSERT INTO technics (id, name, descr) VALUES (19, 'Aikiotoshi', NULL);
INSERT INTO technics (id, name, descr) VALUES (18, 'Sumiotoshi', 'Zu Deutsch: Eckenkippe');
INSERT INTO technics (id, name, descr) VALUES (16, 'Kaiten-nage', 'Zu Deutsch: Schleuderwurf');
INSERT INTO technics (id, name, descr) VALUES (17, 'Juji-garami', 'Zu Deutsch: Kreuz verwickelnde Technik');
INSERT INTO technics (id, name, descr) VALUES (15, 'Soto-kaiten-nage', NULL);
INSERT INTO technics (id, name, descr) VALUES (14, 'Uchi-kaiten-nage', NULL);
INSERT INTO technics (id, name, descr) VALUES (13, 'Koshi-nage', 'Zu Deutsch: Hüftwurf');
INSERT INTO technics (id, name, descr) VALUES (12, 'Udekime-nage', NULL);
INSERT INTO technics (id, name, descr) VALUES (11, 'Kokyu-nage', 'Kokyu nage bedeutet Atemkraft-Wurf. Kokyu Nage sind meist keine richtige Technik und bestehen nur aus einem kleinen Element. Prinzipiell wird nur versucht, die Kraft des Angreifers durchzulassen.');
INSERT INTO technics (id, name, descr) VALUES (10, 'Tenchi-nage', 'Zu Deutsch: Himmel- und Erdewurf');
INSERT INTO technics (id, name, descr) VALUES (9, 'Sokument irimi-nage', NULL);
INSERT INTO technics (id, name, descr) VALUES (8, 'Irimi-nage', 'Zu Deutsch: Innerer Eingangswurf');
INSERT INTO technics (id, name, descr) VALUES (7, 'Shiho-nage', 'Zu Deutsch: Vierrichtungs-Wurf (auch „Schwertwurf“ genannt; Die Bezeichnung Schwertwurf führt bei einigen Lesern zu dem Missverständnis, dass Schwert Shi hieße. Schwert heißt jedoch Ken, Vier heißt Shi und Nage heißt Wurf. Dennoch gibt es bei der Shiho Nage Bewegung viele Ähnlichkeiten zu Bewegungen mit einem Schwert, dies gilt jedoch für sehr viele Bewegungen im Aikido und ist der Tatsache geschuldet, dass der Entwickler des Aikido sich intensiv mit dem Schwertkampf beschäftigt hat.)');
INSERT INTO technics (id, name, descr) VALUES (6, 'Kotegaeshi', 'Zu Deutsch: Handgelenksdrehung und Kipphebel');
INSERT INTO technics (id, name, descr) VALUES (5, 'Gokyo', 'Auch Ude-nobashi genannt.');
INSERT INTO technics (id, name, descr) VALUES (4, 'Yonkyo', 'Auch Tebuki-osae genannt.');
INSERT INTO technics (id, name, descr) VALUES (3, 'Sankyo', 'Auch Kote-hineri genannt.');
INSERT INTO technics (id, name, descr) VALUES (2, 'Nikyo', 'Auch Kote-mawashi genannt.');
INSERT INTO technics (id, name, descr) VALUES (1, 'Ikkyo', 'Armstreckhebel, auch Ude-osae genannt. Die Technik wird als omote (vor dem Partner) und ura (hinter dem Partner) gelehrt.');

-- Index: attack_id
DROP INDEX IF EXISTS attack_id;
CREATE INDEX attack_id ON attacks (id ASC);

-- Index: grade_id
DROP INDEX IF EXISTS grade_id;
CREATE INDEX grade_id ON grades (id ASC);

-- Index: technic_id
DROP INDEX IF EXISTS technic_id;
CREATE INDEX technic_id ON technics (id ASC);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
